# Hosting WordPress On AWS Elastic Beanstalk & RDS [draft]

[AWS Status Page](https://status.aws.amazon.com/)

![Overview](https://www.freecodecamp.org/news/content/images/2020/07/wp-eb-rds-bb-1-1.png)

Wordpress is one of the most popular content management systems (CMS) out there and it is claimed to run **30%** of the internet.

Almost anybody can get a website within minutes using the popular CMS.

The typical workflow would be to install it in your favorite deployment solution, pick your theme and you are good to go.

But, a lot of times, people build upon the initial starting point and add custom features.

This is where you would implement DevOps practices to make it easier for the development team to ship the featues.

DevOps is an abbriviation of 'development' and 'operations'. It combines a set of practices that combines software development (Dev) and IT operations (Ops).

Some abbreviations I will use :

AWS - Amazon Web Services
EB  - AWS Elastic Beanstalk
RDS - AWS Relational Database Servive
CI  - Continuous Integrations
CD  - Continuous Delivery/ Continuous Deployment

In this article we will host Wordpress on EB and RDS.

And we will set up CI/CD with Bitbucket Pipelines.

EB is an orchestration service provided by AWS which orchestrates various AWS services, including EC2, S3, SNS, CloudWatch etc.

Meanwhile, RDS is the distributed relational database service by AWS.

Atlassian Bitbucket with the ecosytem of tools it comes with makes it a very good repository hosting solution.

Here's the content overview :

* Wordpess On EB & RDS
  * Installing the EB CLI
  * Setting up the project directory
  * Creating an EB environment (`production` branch) and network configurations
  * Deploying
  * Setting up `developlment` branch
* CI/CD With Bitbucket Pipelines

## Install The EB CLI

The EB CLI integrates with Git and simplifies the process of creating environments, deploying code changes, and connecting to the instances in your environment with SSH. You will perform all of these activites when installing and configuring WordPress.

If you have `pip`, use it to install the EB CLI.

```Shell
pip install awsebcli
```

Add the local install location to your OS's path variable. The installation path depends on where you installed Python. Note that it is recommended to install Python in your user directory, and avoid using the version of Python that came with your operating system.

### Linux

```Shell
export PATH=~/.local/bin:$PATH
```

### OS-X

```Shell
export PATH=~/Library/Python/3.6/bin:$PATH
```

### Windows

Add `%USERPROFILE%\AppData\Roaming\Python\Scripts` to your PATH variable. Search for **Edit environment variables for your account** in the Start menu.

If you don't have pip, follow the instructions [here](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html).

## Set Up Your Project Directory

1. Download WordPress. [Download page](https://wordpress.org/download/).

```Shell
curl https://wordpress.org/wordpress-5.4.2.tar.gz -o wordpress.tar.gz
```

2. Download the EB configuration files in this repository.

```Shell
wget https://github.com/awslabs/eb-php-wordpress/releases/download/v1.0/eb-php-wordpress-v1.zip
```

3. Extract WordPress and change the name of the folder.

```Shell
tar -xvf wordpress.tar.gz
```

```Shell
cp -r wordpress wordpress-beanstalk-production
```

4. Extract the configuration files over the WordPress installation.

```Shell
unzip eb-php-wordpress-v1.zip -d wordpress-beanstalk-production/
```

```Shell
 creating: wordpress-beanstalk-production/.ebextensions/
inflating: wordpress-beanstalk-production/.ebextensions/dev.config  
inflating: wordpress-beanstalk-production/.ebextensions/efs-create.config  
inflating: wordpress-beanstalk-production/.ebextensions/efs-mount.config  
inflating: wordpress-beanstalk-production/.ebextensions/loadbalancer-sg.config  
inflating: wordpress-beanstalk-production/.ebextensions/wordpress.config  
inflating: wordpress-beanstalk-production/LICENSE  
inflating: wordpress-beanstalk-production/README.md  
inflating: wordpress-beanstalk-production/wp-config.php
```

## Create An EB Environment

1. Configure a local EB CLI repository with the PHP platform. Choose a [supported region](http://docs.aws.amazon.com/general/latest/gr/rande.html#elasticbeanstalk_region) that is close to you.

```Shell
cd wordpress-beanstalk-production
```

```Shell
eb init -i
```

* Choose your required region
  * 7 ap-southeast-1
* Select an application to use
  * 6 Create new application
  * Keep default e.g. wordpress-beanstalk-production
* It appears you are using PHP. Is this correct?
  * Y
* Select a platform branch
  * 5 PHP 7.2 running on 64bit Amazon Linux
* Don't configure CodeCommit
* Select a keypair for SSH
  * 7 Create new KeyPair
  * Type a keypair name : e.g. wordpress-beanstalk-production
  * Passphrase depends on you

2. Create an EB sample environment with a MySQL database.

```Shell
eb create wordpress-beanstalk-production --sample --database
```

```Shell
Enter an RDS DB username (default is "ebroot"): admin
Enter an RDS DB master password:
Retype password to confirm:
Environment details for: wordpress-beanstalk-production
  Application name: wordpress-beanstalk-production
  Region: ap-southeast-1
  Deployed Version: Sample Application
  Environment ID: e-5zxsa5gqh3
  Platform: arn:aws:elasticbeanstalk:ap-southeast-1::platform/PHP 7.2 running on 64bit Amazon Linux/2.9.8
  Tier: WebServer-Standard-1.0
  CNAME: UNKNOWN
  Updated: 2020-07-19 12:31:39.648000+00:00
Printing Status:
2020-07-19 12:31:38    INFO    createEnvironment is starting.
```

Test the deployment

```Shell
eb open
```

You will see this :

### Networking Configuration

* `.ebextensions/efs-create.config` creates an EFS file system and mount points in each Availability Zone / subnet in your VPC. Identify your default VPC and subnet IDs in the [VPC console](https://console.aws.amazon.com/vpc/home?) and replace the values.

**`Default VPC is used for demo purposes.`**

### Deploy WordPress To Your Environment

Delete the `loadbalancer-sg.config` file

```Shell
rm .ebextensions/loadbalancer-sg.config
```

Check the status of the deployment, our desired is 'green'.

```Shell
eb status
```

Deploy using the CLI

```Shell
eb deploy
```

Wordpress will be deployed as a new application version replacing the sample deployment.

Checkout the new site

```Shell
eb open
```

### Install Wordpress

### TODO

### Setting Up The 'development' Environment

1. Creating development directory

```Shell
cp -r wordpress wordpress-beanstalk-development
```

2. Add EB configuration files

```Shell
unzip eb-php-wordpress-v1.zip -d wordpress-beanstalk-development/
```

3. Go into the directory

```Shell
cd wordpress-beanstalk-development
```

4. Initialize the environment

```Shell
eb init -i
```

* Choose your required region
  * 7 ap-southeast-1
* Select an application to use
  * 6 Create new application
  * Keep default e.g. wordpress-beanstalk-development
* It appears you are using PHP. Is this correct?
  * Y
* Select a platform branch
  * 5 PHP 7.2 running on 64bit Amazon Linux
* Don't configure CodeCommit
* Select a keypair for SSH
  * Create new KeyPair
  * Type a keypair name : e.g. wordpress-beanstalk-development
  * Passphrase depends on you

5. Create the environment

```Shell
eb create wordpress-beanstalk-development --sample
```

`We are not including the --database arguement because we will be configuring it manually to the production database.`

After it is successfully created, check status.

```Shell
eb status
```

6. Delete `loadbalancer-sg.config` file && replace `.ebextensions/efs-create.config` VPC values

```Shell
rm .ebextensions/loadbalancer-sg.config
```

7. Update the environment variables from the EB console for RDS

![Screenshot-from-2020-07-29-21-17-00](https://www.freecodecamp.org/news/content/images/2020/07/Screenshot-from-2020-07-29-21-17-00.png)

#### TODO : link to reference

8. Update security groups

In EB console

* Go to 'elastic-beanstalk-development' application
* Select 'Configuration' from the left column
* 'Edit' Instances
* Take note of the applied 'Group ID'

In RDS

* Select the RDS instance created with 'elastic-beanstalk-production'
* Click on the 'VPC security groups'
* Click on the applied 'Security Group ID'
* 'Edit inbound rules'
* 'Add rule'
* 'MYSQL/Aurora'
* You may type out the relevant security group ID
* 'Save rules'

9. Deploy

```Shell
eb deploy
```

10. Check the URL for expected changes

```Shell
eb open
```

Now, we have two identical environments with two applications pointing to the same database instance.

You may add another `branch` for staging or testing or whatever your needs are. The steps would be similar.

Next step is to configure EB with Bitbucket repository.

## CI/CD With Bitbucket Pipelines

### Setting Up The Repository

* Create a repository. For examples : `wordpress-beanstalk`
* Include a README? 'No'
* In the `wordpress-beanstalk-production` directory initialize git

```Shell
git init
```

* Add the remote repository

```Shell
git remote add origin https://johndoe@bitbucket.org/johndoe/wordpress-beanstalk.git
```

* Add your local files to git

```Shell
git add .
```

Make an initial commit

```Shell
git commit -m "Add all the files."
```

* Push your files to the repository

```Shell
git push -u origin master
```

Create 'production' branch and switch to it

```Shell
git checkout -b production
```

Create 'development' branch by copying `production`

```Shell
git branch -c development
```

Add, commit and push your changes to the remote repository

```Shell
git add .
git commit -m "Add files."
git push origin production
git checkout development
git add . commit -m "Add files."
git push origin development
```

Now, we have identical files for both our branches and we need to modify some files to reflect separate EB applications.

For this, we copy the `.elasticbeanstalk/config.yml` of `wordpress-beanstalk-development` directory into the branch `development`.

Before pushing the changes, delete the following lines from `.gitignore`.

```Shell
.elasticbeanstalk/*
!.elasticbeanstalk/*.cfg.yml
!.elasticbeanstalk/*.global.yml
```

And we are good to go

```Shell
git add .
git commit -m "Changed .elasticbeanstalk/config.yml to reflect wordpress-beanstalk-development"
git push origin development
```

(Modify the `.gitignore` file for `production` branch as well.)

### Creating An AWS IAM Role

IAM Role enables us to work securely with from Bitbucket.

* Go to IAM from AWS console
* 'Users'
* 'Add user'
* 'User name'
  * 'bitbucket'
* 'Programmatic access'
* 'Attach existing policies directly'
* 'AWSElasticBeanstalkFullAccess'
* Skip tags
* 'Create user'
* Save the access key ID and secret access key

### Adding Environment Variables

Go to 'Repository Settings' -> 'Repository Variables' -> Enable the pipelines in settings if required and then add this key-value pairs

* AWS_ACCESS_KEY_ID : From the IAM user
* AWS_SECRET_ACCESS_KEY : From the IAM user
* AWS_DEFAULT_REGION : The one you are using. For me it is ap-southeast-1
* ENVIRONMENT_NAME_PROD : wordpress-beanstalk-production
* ENVIRONMENT_NAME_DEV : wordpress-beanstalk-development

![Screenshot-of-repository-variables](https://www.freecodecamp.org/news/content/images/2020/07/Screenshot-from-2020-07-25-21-33-39.png)

### Adding the `bitbucket-pipelines.yml`

The YAML file should be placed in the main directory. Make `bitbucket-pipelines.yml` file in your local directory with the following content and push it to the repository (for both environments). It will trigger the pipeline.

```Shell
image: tyrellsys/aws-ebcli

pipelines:
  branches:
    production:
      - step:
          script:
            - eb init --region $AWS_DEFAULT_REGION --platform php-7.2
            - eb deploy $ENVIRONMENT_NAME_PROD --label production
    development:
      - step:
          script:
            - eb init --region $AWS_DEFAULT_REGION --platform php-7.2
            - eb deploy $ENVIRONMENT_NAME_DEV --label dev
```

![Screenshot-from-2020-07-25-21-42-36](https://www.freecodecamp.org/news/content/images/2020/07/Screenshot-from-2020-07-25-21-42-36.png)

If you visit `Pipelines` you may see that your first deployment failed.

This is something I spent a lot of time trying to figure out and had to come with a work around.

What happens is, you cannot just `eb deploy` like you used to from your local machine. Instead you have to provide a previous application version name.

Hence

```Shell
eb deploy $ENVIRONMENT_NAME_PROD --label production
```

Do an deployment from the local machine with the following command

```Shell
eb deploy --label production
```

And rerun the pipeline.

![Screenshot-failed-deployment](https://www.freecodecamp.org/news/content/images/2020/07/Screenshot-from-2020-07-25-21-59-36.png)

I have included links to Stackoverflow questions I asked about it in the references.

And after the rerun, you will get

![Screenshot-successful-production](https://www.freecodecamp.org/news/content/images/2020/07/Screenshot-from-2020-07-25-22-01-20.png)

This was our `production` branch. Try making minor changes to `development branch` and it will deploy as such. (You still have to make a local deployment with `--label dev` before this step)

![Screenshot-successful-development](https://www.freecodecamp.org/news/content/images/2020/07/Screenshot-from-2020-07-25-22-13-32.png)

### TODO 6

AUTH_KEY: 'test'
SECURE_AUTH_KEY: 'test'
LOGGED_IN_KEY: 'test'
NONCE_KEY: 'test'
AUTH_SALT: 'test'
SECURE_AUTH_SALT: 'test'
NONCE_SALT: 'test'

## Conclusion

We just hosted Wordpress in AWS Elastic Beanstalk and RDS along with two branches for production and development.

We also configured Bitbucket Pipelines for CI/CD!

Congrats to you!

If you have any feedback you may reach out to me on [Twitter](twitter.com/omar161000) and connect on [LinkedIn](https://www.linkedin.com/in/omar16100).

I also have a few blog posts on my [website](omar.ai/).

## References

* [PHP High Available Wordpress Tutorial](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/php-hawordpress-tutorial.html)
* [EB-CLI-PHP-Wordpress](https://github.com/aws-samples/eb-php-wordpress)

Somewhat related :

* <https://www.reddit.com/r/aws/comments/azbww9/wordpress_workflow_elastic_beanstalk_bitbucket/>
* <https://stackoverflow.com/questions/55231929/bitbucket-pipeline-with-aws-eb-and-wordpress>
* <https://medium.com/@avishayil/deploy-to-elastic-beanstalk-using-bitbucket-pipelines-189eb75cf052>

### Debug

* AWS Linux 2 AMI has issues with EB deployments
  * This [Github issue](https://github.com/awsdocs/elastic-beanstalk-samples/issues/120#issuecomment-636009209) is not solved, that's wht used the Linux 1 AMIs.
* [AWS 'eb init' not a valid key=value pair (missing equal-sign) Bitbucket pipelines](https://stackoverflow.com/questions/62860407/aws-eb-init-not-a-valid-key-value-pair-missing-equal-sign-bitbucket-pipeline)
* [AWS Beanstalk error “InvalidParameterValueError: No Application Version named 'ap…7' found.” Bitbucket pipelines](https://stackoverflow.com/questions/62868950/aws-beanstalk-error-invalidparametervalueerror-no-application-version-named-a)
